# Lancer le projet

- Cloner ce repo : https://gitlab.com/cda2021/refuelandgo.git
- Cloner le repo de l'api : https://gitlab.com/cda2021/refuelandgoapi.git

- Dans le dossier de l'api executer les commandes suivantes :  (⚠ Node JS requis)
    - ```npm install``` (vous devez possèder npm au préalable)
    - ```node index.js``` qui va lancer un serveur Node JS
    
- Créer un fichier au chemin suivant : ```app/src/main/res/raw/env.properties```
- Ajouter dans ce fichier l'adresse du serveur de l'api par exemple : ```server_url=http://adresse_ip_locale:3000/```
- Installer les dépendances build.gradle (app)
- Lancez l'application

## Mails
- En lançant le serveur NodeJS, un serveur de mails catching se lancera également, afin de récupérer les mails envoyés depuis le serveur.
Ils sont visibles à l'adresse http://localhost:1080

## Authentification

4 comptes sont disponibles (hors inscription) :
    - oucema@gmail.com / password
    - remi@gmail.com / password
    - guillaume@gmail.com / password
    - gilles@gmail.com / password
package com.afti.refuelandgo.data.model;

public class Port
{
    protected String id;
    protected String name;
    protected String distance;
    protected String price;

    public Port(String id, String name, String distance, String price)
    {
        this.id = id;
        this.name = name;
        this.distance = distance;
        this.price = price;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDistance() { return distance; }

    public void setDistance(String distance) { this.distance = distance; }

    public String getPrice() {
        return price;
    }

    public String getFullPrice() {
        return price + " €";
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package com.afti.refuelandgo.data;

import android.os.AsyncTask;
import android.util.Log;

import com.afti.refuelandgo.data.model.LoggedInUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    private final OkHttpClient client = new OkHttpClient();

    public Result<LoggedInUser> login(String username, String password, String url) {

        AsyncTask task = new Authenticate().execute(url, username, password);

        try {
            String result = task.get().toString();
            Log.i("-------[DEBUG HTTP]-------", result);

            JSONArray jsonArray = new JSONArray(result);
            JSONObject jsonObject = new JSONObject(jsonArray.getString(0));

            boolean success = Boolean.parseBoolean(jsonObject.getString("success"));

            if (success) {
                JSONObject user = new JSONObject(jsonObject.getString("user"));
               LoggedInUser loggedInUser = new LoggedInUser(user.getString("id"), user.getString("username"), jsonObject.getString("token"), user.toString());
               return new Result.Success<>(loggedInUser);
            }


        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }

        return new Result.Error(new IOException("Error logging in"));
    }

    private class Authenticate extends AsyncTask<String, String, String>
    {
        protected String doInBackground(String... param)
        {
            String result = "";
            JSONObject body = new JSONObject();

            try {
                body.put("email", param[1]);
                body.put("password", param[2]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody jsonBody = RequestBody.create(MediaType.parse("application/json"), body.toString());

            Request request = new Request.Builder()
                    .url(param[0])
                    .post(jsonBody)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                result = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
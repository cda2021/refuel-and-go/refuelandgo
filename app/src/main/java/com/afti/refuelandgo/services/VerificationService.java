package com.afti.refuelandgo.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class VerificationService {
    private final OkHttpClient client = new OkHttpClient();
    private String id;
    private String code;

    public VerificationService(String id, String code) {
        this.id = id;
        this.code = code;
    }

    public void verify(Context context) throws Exception {
        String url = EnvService.get("server_url", context) + "/user-verification";
        AsyncTask task = new Verification().execute(url, id, code);

        String result = task.get().toString();
        Log.i("-------[DEBUG HTTP]-------", result);
        JSONObject jsonObject = new JSONObject(result);
        boolean success = Boolean.parseBoolean(jsonObject.getString("success"));

        if (!success) {
            throw new Exception(jsonObject.getString("error"));
        }

        JSONObject user = jsonObject.getJSONObject("user");

        SharedPreferences preferences = context.getSharedPreferences("default", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user-token", user.getString("token"));
        editor.putString("user-json", user.toString());
        editor.apply();
    }

    private class Verification extends AsyncTask<String, String, String> {
        protected String doInBackground(String... param) {
            String result = "";
            JSONObject body = new JSONObject();

            try {
                body.put("userId", param[1]);
                body.put("verificationCode", param[2]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody jsonBody = RequestBody.create(MediaType.parse("application/json"), body.toString());

            Request request = new Request.Builder()
                    .url(param[0])
                    .post(jsonBody)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                result = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }
    }
}

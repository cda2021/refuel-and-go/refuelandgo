package com.afti.refuelandgo.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SettingsService {

    private final OkHttpClient client = new OkHttpClient();
    private String id;
    private String unit;
    private String distance;

    public SettingsService(String id, String unit, String distance) {
        this.id = id;
        this.unit = unit;
        this.distance = distance;
    }

    public void send(Context context) throws Exception {
        String url = EnvService.get("server_url", context) + "/user/" + id;
        AsyncTask task = new Settings().execute(url, unit, distance);
        String result = task.get().toString();

        Log.i("-------[DEBUG HTTP]-------", result);
        JSONObject jsonObject = new JSONObject(result);
        boolean success = Boolean.parseBoolean(jsonObject.getString("success"));

        if (!success) {
            throw new Exception(jsonObject.getString("error"));
        }

        JSONObject user = jsonObject.getJSONObject("user");

        SharedPreferences preferences = context.getSharedPreferences("default", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user-json", user.toString());
        editor.apply();
    }

    private class Settings extends AsyncTask<String, String, String> {
        protected String doInBackground(String... param) {
            String result = "";
            JSONObject body = new JSONObject();

            try {
                body.put("unit", param[1]);
                body.put("distance", param[2]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody jsonBody = RequestBody.create(MediaType.parse("application/json"), body.toString());

            Request request = new Request.Builder()
                    .url(param[0])
                    .put(jsonBody)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                result = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }
    }
}

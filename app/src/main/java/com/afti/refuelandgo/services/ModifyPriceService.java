package com.afti.refuelandgo.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ModifyPriceService {
    private final OkHttpClient client = new OkHttpClient();
    private String id;
    private Double price;

    public ModifyPriceService(String id, Double price) {
        this.id = id;
        this.price = price;
    }

    public void edit(Context context) throws Exception {
        String url = EnvService.get("server_url", context) + "/ports/" + id;
        AsyncTask task = new Modification().execute(url, id, Double.toString(price));
        String result = task.get().toString();
        Log.i("-------[DEBUG HTTP]-------", result);
        JSONObject jsonObject = new JSONObject(result);
        boolean success = Boolean.parseBoolean(jsonObject.getString("success"));

        if (!success) {
            throw new Exception(jsonObject.getString("error"));
        }
    }

    private class Modification extends AsyncTask<String, String, String> {
        protected String doInBackground(String... param) {
            String result = "";
            JSONObject body = new JSONObject();

            try {
                body.put("id", param[1]);
                body.put("price", Double.parseDouble(param[2]));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody jsonBody = RequestBody.create(MediaType.parse("application/json"), body.toString());

            Request request = new Request.Builder()
                    .url(param[0])
                    .put(jsonBody)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                result = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }
    }
}

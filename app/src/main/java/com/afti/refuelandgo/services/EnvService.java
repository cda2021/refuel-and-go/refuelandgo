package com.afti.refuelandgo.services;

import android.content.Context;
import android.util.Log;

import com.afti.refuelandgo.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class EnvService
{
    public static String get(String key, Context context)
    {
        Properties properties = new Properties();
        InputStream inputStream = context.getResources().openRawResource(R.raw.env);
        try {
            properties.load(inputStream);
            return properties.getProperty(key);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}

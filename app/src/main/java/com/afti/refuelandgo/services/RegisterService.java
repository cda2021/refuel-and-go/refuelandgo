package com.afti.refuelandgo.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.afti.refuelandgo.data.LoginDataSource;
import com.afti.refuelandgo.data.Result;
import com.afti.refuelandgo.data.model.LoggedInUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterService {
    private final OkHttpClient client = new OkHttpClient();
    private String email;
    private String username;
    private String password;

    final String VERIFICATION_OK = "ok";
    final String VERIFICATION_ERROR = "error";

    public RegisterService(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public Boolean verify() throws Exception {
        // Logique de vérification du formulaire

        if (this.email.length() < 3) {
            throw new Exception("Email length should be at least 3.");
        }

        if (this.username.length() < 3) {
            throw new Exception("Username length should be at least 3.");
        }

        if (this.password.length() < 3) {
            throw new Exception("Password length should be at least 3.");
        }

        return true;
    }

    public void register(Context context) throws Exception {
        String url = EnvService.get("server_url", context) + "/user";

        AsyncTask task = new Registration().execute(url, username, email, password);

        String result = task.get().toString();
        Log.i("-------[DEBUG HTTP]-------", result);
        JSONObject jsonObject = new JSONObject(result);
        boolean success = Boolean.parseBoolean(jsonObject.getString("success"));
        JSONObject user = jsonObject.getJSONObject("user");
        Log.i("-------[DEBUG USER JSON]-------", user.toString());


        if (!success) {
            throw new Exception(jsonObject.getString("error"));
        }

        SharedPreferences preferences = context.getSharedPreferences("default", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user-registered-id", user.getString("id"));
        editor.apply();
    }

    private class Registration extends AsyncTask<String, String, String> {
        protected String doInBackground(String... param) {
            String result = "";
            JSONObject body = new JSONObject();

            try {
                body.put("username", param[1]);
                body.put("email", param[2]);
                body.put("password", param[3]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            RequestBody jsonBody = RequestBody.create(MediaType.parse("application/json"), body.toString());

            Request request = new Request.Builder()
                    .url(param[0])
                    .post(jsonBody)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                result = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }
    }
}

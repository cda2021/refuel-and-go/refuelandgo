package com.afti.refuelandgo.ui.register;

import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.afti.refuelandgo.R;
import com.afti.refuelandgo.services.RegisterService;
import com.google.android.material.snackbar.Snackbar;

public class Register extends Fragment {

    public Register() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root =  inflater.inflate(R.layout.fragment_register, container, false);
        Button buttonRegister = root.findViewById(R.id.register_submit);
        final EditText usernameEditText = root.findViewById(R.id.username);
        final EditText emailEditText = root.findViewById(R.id.email);
        final EditText passwordEditText = root.findViewById(R.id.password);


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameEditText.getText().toString();
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                RegisterService service = new RegisterService(email, username, password);

                try {
                    service.verify();
                    service.register(getContext());
                    Navigation.findNavController(root).navigate(R.id.action_nav_register_to_verificationFragment);
                } catch (Exception e) {
                    Log.i("-------[DEBUG REGISTER]-------", e.getMessage());
                    Snackbar.make(getView(), e.getMessage(), Snackbar.LENGTH_LONG)
                            .setBackgroundTintMode(PorterDuff.Mode.DARKEN)
                            .setBackgroundTint(ContextCompat.getColor(getContext(), R.color.design_default_color_error))
                            .setAction("Action", null).show();
                }
            }
        });

        return root;
    }
}
package com.afti.refuelandgo.ui.explore;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorSpace;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.afti.refuelandgo.R;
import com.afti.refuelandgo.data.model.Port;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListAdapter extends ArrayAdapter<Port>
{
    private static final String LOG_TAG = ListAdapter.class.getSimpleName();


    public ListAdapter(Activity context, ArrayList<Port> list)
    {
        super(context,0,list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View listItemView = convertView;
        if (listItemView == null)
        {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.layout_list_port, parent, false);
        }

        Port unPort = getItem(position);
        TextView nameTv = (TextView) listItemView.findViewById(R.id.namePortList);
        TextView distanceTv = (TextView) listItemView.findViewById(R.id.distancePortList);
        TextView price = (TextView) listItemView.findViewById(R.id.pricePortList);

        nameTv.setText(unPort.getName());
        distanceTv.setText(unPort.getDistance());
        price.setText(unPort.getFullPrice());

        return listItemView;
    }
}

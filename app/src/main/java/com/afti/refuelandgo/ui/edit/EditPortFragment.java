package com.afti.refuelandgo.ui.edit;

import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.EditTextPreference;

import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afti.refuelandgo.ExploreActivity;
import com.afti.refuelandgo.R;
import com.afti.refuelandgo.services.ModifyPriceService;
import com.afti.refuelandgo.ui.explore.ListFragment;
import com.google.android.material.snackbar.Snackbar;

import java.util.regex.Pattern;

import static java.lang.Double.parseDouble;

public class EditPortFragment extends Fragment
{
    protected String portName = "";
    protected String portId = "";
    protected float portPrice;
    protected EditText priceText;
    public static final Pattern PRICE = Pattern.compile("^\\d+\\.?\\d*$");

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_port_fragment, container, false);
    }

    protected void SnackBarError (String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_LONG)
                .setBackgroundTintMode(PorterDuff.Mode.DARKEN)
                .setBackgroundTint(ContextCompat.getColor(getContext(), R.color.design_default_color_error))
                .setAction("Action", null).show();

    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("----------DEBUG EDIT--------", getArguments().getString("portName"));
        portName = getArguments().getString("portName");
        portPrice = getArguments().getFloat("portPrice");
        portId = getArguments().getString("portId");

        ExploreActivity activity = (ExploreActivity) getActivity();
        activity.setActivityTitle(portName);

        Button validate = getView().findViewById(R.id.button_edit_valid);
        priceText = (EditText) getView().findViewById(R.id.editCarbPrice);
        priceText.setText(String.valueOf(portPrice));

        getActivity().setTitle(portName);
        validate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Récupération des informations saisie
                // System.out.println("contenu du texte : " + priceText);
                String price = priceText.getText().toString();

                if (TextUtils.isEmpty(price)) {
                    SnackBarError("Veuillez remplir le nouveau prix");
                }
                else
                {
                    if (PRICE.matcher(price).matches()) {
                        SnackBarError("Enregistrement...");
                        if (modifyPrice(parseDouble(price))) {
                            NavHostFragment.findNavController(EditPortFragment.this)
                                    .navigate(R.id.action_editPortFragment_to_FirstFragment);
                        }
                    }
                    else {
                        SnackBarError("La saisie du prix est incorrecte.");
                    }
                }
            }
        });

    }

    private boolean modifyPrice(Double price) {
        ModifyPriceService modify = new ModifyPriceService(portId, price);
        try {
            modify.edit(getContext());
            Snackbar.make(getView(), "The price has been edited with succeed.", Snackbar.LENGTH_LONG)
                    .setBackgroundTintMode(PorterDuff.Mode.LIGHTEN)
                    .setBackgroundTint(ContextCompat.getColor(getContext(), R.color.design_default_color_primary))
                    .setAction("Action", null).show();
            return true;
        } catch (Exception e) {
            SnackBarError(e.getMessage());
            return false;
        }
    }
}

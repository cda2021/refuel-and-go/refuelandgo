package com.afti.refuelandgo.ui.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.fragment.NavHostFragment;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.afti.refuelandgo.R;
import com.afti.refuelandgo.services.SettingsService;
import com.afti.refuelandgo.ui.explore.ListFragment;

import org.json.JSONException;
import org.json.JSONObject;

public class SettingsFragment extends PreferenceFragmentCompat {
    private static final String TAG = "--- DEBUG PREFERENCES ---";
    EditTextPreference distance;
    ListPreference unit;
    JSONObject userJson;

   @Override
    public void onPrepareOptionsMenu (Menu menu) {
       menu.findItem(R.id.action_settings).setEnabled(false);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
        SharedPreferences preferences = getContext().getSharedPreferences("default", Context.MODE_PRIVATE);
        try {
            userJson = new JSONObject(preferences.getString("user-json", null));
            distance = findPreference("rayon");
            unit = findPreference("unit");
            distance.setText(userJson.getString("searchRadius"));
            unit.setValue(userJson.getString("unit"));
            Log.i(TAG, userJson.toString());
        } catch (JSONException e) {
            Log.i(TAG, e.getMessage());
        }


        distance.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                try {
                    editSettings(unit.getValue(), newValue.toString());
                    return true;
                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });

        unit.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                try {
                    editSettings(newValue.toString(), distance.getText());
                    return true;
                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        });

        if (distance != null) {
            distance.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                @Override
                public void onBindEditText(@NonNull EditText editText) {
                    editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                }
            });
        }
    }

    protected void editSettings(String unit, String distance) throws Exception {
       SharedPreferences preferences = getContext().getSharedPreferences("default", Context.MODE_PRIVATE);
       userJson = new JSONObject(preferences.getString("user-json", null));
       String userId = userJson.getString("id");
       SettingsService settingsService = new SettingsService(userId, unit, distance);
       settingsService.send(getContext());
    }
}
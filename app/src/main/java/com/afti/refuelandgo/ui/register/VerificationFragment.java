package com.afti.refuelandgo.ui.register;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afti.refuelandgo.ExploreActivity;
import com.afti.refuelandgo.R;
import com.afti.refuelandgo.services.VerificationService;
import com.google.android.material.snackbar.Snackbar;

public class VerificationFragment extends Fragment {

    public VerificationFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_verification, container, false);
        EditText verificationCodeEdit = root.findViewById(R.id.verification_code_input);
        Button submit = root.findViewById(R.id.verification_code_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = verificationCodeEdit.getText().toString();
                Log.i("-------[DEBUG CODE VERIFICATION]-------", code);
                SharedPreferences preferences = getContext().getSharedPreferences("default", Context.MODE_PRIVATE);
                String userId = preferences.getString("user-registered-id", null);
                VerificationService service = new VerificationService(userId, verificationCodeEdit.getText().toString());

                try {
                    service.verify(getContext());
                    Intent intent = new Intent(getContext(), ExploreActivity.class);
                    startActivity(intent);
                } catch (Exception e) {
                    Snackbar.make(getView(), e.getMessage(), Snackbar.LENGTH_LONG)
                            .setBackgroundTintMode(PorterDuff.Mode.DARKEN)
                            .setBackgroundTint(ContextCompat.getColor(getContext(), R.color.design_default_color_error))
                            .setAction("Action", null).show();
                }
            }
        });

        return root;
    }
}
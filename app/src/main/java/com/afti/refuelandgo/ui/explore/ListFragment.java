package com.afti.refuelandgo.ui.explore;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.afti.refuelandgo.R;
import com.afti.refuelandgo.data.model.Port;
import com.afti.refuelandgo.services.EnvService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class ListFragment extends Fragment implements LocationListener {
    private final OkHttpClient client = new OkHttpClient();
    private ListView lvPort;
    private EditText etSearch;
    private ArrayList<Port> mesPorts;
    private String maRecherhe = "";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        lvPort = view.findViewById(R.id.listPort);
        etSearch = view.findViewById(R.id.editTextSearch);

        lvPort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Port port = mesPorts.get(position);
                Bundle result = new Bundle();
                result.putString("portName", port.getName());
                result.putFloat("portPrice", Float.parseFloat(port.getPrice()));
                result.putString("portId", port.getId());
                NavHostFragment.findNavController(ListFragment.this).navigate(R.id.action_FirstFragment_to_editPortFragment, result);
            }
        });

        getLocalization();

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                try {
                    SharedPreferences preferences = getContext().getSharedPreferences("default", MODE_PRIVATE);
                    JSONObject user = new JSONObject(preferences.getString("user-json", null));
                    maRecherhe = etSearch.getText().toString();
                    getLocalization();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {

        lvPort.deferNotifyDataSetChanged();
        mesPorts = new ArrayList<Port>();

        double lon,lat;

        if (null == location) {
            lon = 49.4944;
            lat = 0.1079;
        } else {
            lon = location.getLongitude();
            lat = location.getLatitude();
        }

        try {
            String url = EnvService.get("server_url", getContext());
            AsyncTask task;
            SharedPreferences preferences = getContext().getSharedPreferences("default", MODE_PRIVATE);
            JSONObject user = new JSONObject(preferences.getString("user-json", null));
            String userRadius = user.getString("searchRadius");

            if (null == userRadius) {
                userRadius = "16";
            }

            if (maRecherhe.matches("")) {
                task = new callAPI().execute(url + "/ports-in-radius/"+ lat + "/" + lon + "/" + userRadius);
            } else {
                task = new callAPI().execute(url + "/ports/search/"+ lat + "/" + lon + "/" + maRecherhe);
            }

            String listPort = task.get().toString();
            JSONArray listPortJson = new JSONArray(listPort);

            for (int i = 0; i < listPortJson.length(); i++)
            {
                JSONObject unPortJson = listPortJson.getJSONObject(i);
                String distance;
                double floatDistance = Float.parseFloat(unPortJson.optString("distance"));

                if (user.getString("unit").equals("km")) {
                    distance = unPortJson.optString("distance") + " km";
                } else {
                    DecimalFormat df = new DecimalFormat("#.00");
                    distance = df.format(floatDistance * 1.6) + "milles";
                }

                mesPorts.add(new Port(unPortJson.optString("id"), unPortJson.optString("name"), distance, unPortJson.optString("gasPrice")));
            }

            ListAdapter listAdapter = new ListAdapter(this.getActivity(), mesPorts);
            lvPort.setAdapter(listAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    private class callAPI extends AsyncTask<String, String, String> {
        protected String doInBackground(String... param) {
            String result = "";


           // System.out.println("c" + String.valueOf(Double.parseDouble(param[1])));

            /*RequestBody formBody = new FormBody.Builder()
                    .add("currentlat", param[1])
                    .add("currentlon", param[2])
                    .build();*/
            Request request = new Request.Builder()
                    .url(param[0])
                    //.post(formBody)
                    .build();

            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                result = response.body().string();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

    }

    public void getLocalization() {

        LocationManager locationManager = (LocationManager) this.getContext().getSystemService(Context.LOCATION_SERVICE);

        int permissionCheck = ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION);

        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // ask permissions here using below code
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }

        if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }

        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        onLocationChanged(location);
    }
}